package com.kwabenaberko.weatheroo.weather.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.kwabenaberko.weatheroo.core.domain.model.Result
import com.kwabenaberko.weatheroo.weather.domain.error.GetWeatherError
import com.kwabenaberko.weatheroo.weather.domain.model.CurrentWeather
import com.kwabenaberko.weatheroo.weather.domain.model.ForecastWeather
import com.kwabenaberko.weatheroo.weather.domain.model.WeatherIcon
import com.kwabenaberko.weatheroo.weather.domain.usecase.GetWeatherForCityUseCase
import com.kwabenaberko.weatheroo.weather.factory.LocationTestFactory
import com.kwabenaberko.weatheroo.weather.factory.WeatherTestFactory
import com.kwabenaberko.weatheroo.weather.presentation.model.CurrentWeatherUiModel
import com.kwabenaberko.weatheroo.weather.presentation.model.ForecastWeatherUiModel
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.verifySequence
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before

import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class WeatherViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()


    private val mockGetWeatherForCityUseCase = mockk<GetWeatherForCityUseCase>()
    private val mockMapDomainCurrentWeatherToUiModel =
        mockk<(CurrentWeather) -> CurrentWeatherUiModel>()
    private val mockMapDomainForecastWeatherToUiModel =
        mockk<(ForecastWeather) -> ForecastWeatherUiModel>()
    private val mockStateObserver = mockk<Observer<WeatherViewState>>(relaxUnitFun = true)
    private lateinit var viewModel: WeatherViewModel

    @Before
    fun setUp() {
        viewModel = WeatherViewModel(
            getWeatherForCityUseCase = mockGetWeatherForCityUseCase,
            mapDomainCurrentWeatherToUiModel = mockMapDomainCurrentWeatherToUiModel,
            mapDomainForecastWeatherToUiModel = mockMapDomainForecastWeatherToUiModel
        )
        viewModel.viewState.observeForever(mockStateObserver)
    }

    @After
    fun tearDown() {
        viewModel.viewState.removeObserver(mockStateObserver)
    }


    @Test
    fun `should emit initial state on subscribe`() =
        coroutineTestRule.testDispatcher.runBlockingTest {
            verifySequence {
                mockStateObserver.onChanged(WeatherViewState.Initial)
            }
        }

    @Test
    fun `should emit error state if an error occurs when retrieving weather`() =
        coroutineTestRule.testDispatcher.runBlockingTest {
            coEvery { mockGetWeatherForCityUseCase(any(), any()) }
                .returns(Result.Fail(GetWeatherError.UnknownError))

            viewModel.getWeatherForCity(city = "")

            verifySequence {
                mockStateObserver.onChanged(WeatherViewState.Initial)
                mockStateObserver.onChanged(WeatherViewState.Loading)
                mockStateObserver.onChanged(ofType(WeatherViewState.Failed::class))
            }

        }

    @Test
    fun `should emit weather if weather retrieval is successful`() =
        coroutineTestRule.testDispatcher.runBlockingTest {
            val location = LocationTestFactory.makeLocation()
            val currentWeatherUiModel = WeatherTestFactory.makeCurrentWeatherUiModel()
            val forecastWeatherUiModelList = listOf(
                WeatherTestFactory.makeForecastWeatherUiModel()
            )
            coEvery { mockGetWeatherForCityUseCase(any(), any()) }
                .returns(Result.Ok(WeatherTestFactory.makeWeather(location = location)))
            every { mockMapDomainCurrentWeatherToUiModel(any()) }.returns(currentWeatherUiModel)
            every { mockMapDomainForecastWeatherToUiModel(any()) }
                .returns(forecastWeatherUiModelList.first())


            viewModel.getWeatherForCity(city = "")

            verifySequence {
                mockStateObserver.onChanged(WeatherViewState.Initial)
                mockStateObserver.onChanged(WeatherViewState.Loading)
                mockStateObserver.onChanged(
                    WeatherViewState.Content(
                        location = "${location.name}, ${location.country}",
                        currentWeather = currentWeatherUiModel,
                        forecastWeatherList = forecastWeatherUiModelList
                    )
                )
            }

        }


    @Test
    fun `should emit error state if an error occurs when refreshing weather`() =
        coroutineTestRule.testDispatcher.runBlockingTest {
            val location = LocationTestFactory.makeLocation()
            val weather = WeatherTestFactory.makeWeather(location = location)
            val currentWeatherUiModel = WeatherTestFactory.makeCurrentWeatherUiModel()
            val forecastWeatherUiModelList = listOf(
                WeatherTestFactory.makeForecastWeatherUiModel()
            )

            coEvery { mockGetWeatherForCityUseCase(any(), any()) }
                .returns(Result.Ok(weather))
                .andThen(Result.Fail(GetWeatherError.LocationNotFoundError))
            every { mockMapDomainCurrentWeatherToUiModel(any()) }.returns(currentWeatherUiModel)
            every { mockMapDomainForecastWeatherToUiModel(any()) }
                .returns(forecastWeatherUiModelList.first())


            viewModel.getWeatherForCity(city = "")
            viewModel.refreshWeather()

            verifySequence {
                mockStateObserver.onChanged(WeatherViewState.Initial)
                mockStateObserver.onChanged(WeatherViewState.Loading)
                mockStateObserver.onChanged(
                    WeatherViewState.Content(
                        location = "${location.name}, ${location.country}",
                        currentWeather = currentWeatherUiModel,
                        forecastWeatherList = forecastWeatherUiModelList
                    )
                )
                mockStateObserver.onChanged(
                    WeatherViewState.Content(
                        isRefreshing = true,
                        location = "${location.name}, ${location.country}",
                        currentWeather = currentWeatherUiModel,
                        forecastWeatherList = forecastWeatherUiModelList
                    )
                )
                mockStateObserver.onChanged(ofType(WeatherViewState.Failed::class))
            }

        }



    @Test
    fun `should emit weather if refreshing weather was successful`() =
        coroutineTestRule.testDispatcher.runBlockingTest {
            val location = LocationTestFactory.makeLocation()
            val weather = WeatherTestFactory.makeWeather(location = location)
            val currentWeatherUiModel = WeatherTestFactory.makeCurrentWeatherUiModel()
            val forecastWeatherUiModelList = listOf(
                WeatherTestFactory.makeForecastWeatherUiModel()
            )

            coEvery { mockGetWeatherForCityUseCase(any(), any()) }
                .returns(Result.Ok(weather))
            every { mockMapDomainCurrentWeatherToUiModel(any()) }.returns(currentWeatherUiModel)
            every { mockMapDomainForecastWeatherToUiModel(any()) }
                .returns(forecastWeatherUiModelList.first())


            viewModel.getWeatherForCity(city = "")
            viewModel.refreshWeather()

            verifySequence {
                mockStateObserver.onChanged(WeatherViewState.Initial)
                mockStateObserver.onChanged(WeatherViewState.Loading)
                mockStateObserver.onChanged(
                    WeatherViewState.Content(
                        location = "${location.name}, ${location.country}",
                        currentWeather = currentWeatherUiModel,
                        forecastWeatherList = forecastWeatherUiModelList
                    )
                )
                mockStateObserver.onChanged(
                    WeatherViewState.Content(
                        isRefreshing = true,
                        location = "${location.name}, ${location.country}",
                        currentWeather = currentWeatherUiModel,
                        forecastWeatherList = forecastWeatherUiModelList
                    )
                )
                mockStateObserver.onChanged(
                    WeatherViewState.Content(
                        location = "${location.name}, ${location.country}",
                        currentWeather = currentWeatherUiModel,
                        forecastWeatherList = forecastWeatherUiModelList
                    )
                )
            }

        }


}



