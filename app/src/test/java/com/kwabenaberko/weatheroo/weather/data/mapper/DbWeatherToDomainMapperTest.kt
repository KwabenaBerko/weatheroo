package com.kwabenaberko.weatheroo.weather.data.mapper

import com.kwabenaberko.weatheroo.weather.data.db.model.DbLocation
import com.kwabenaberko.weatheroo.weather.data.db.model.DbCurrentWeather
import com.kwabenaberko.weatheroo.weather.data.db.model.DbForecastWeather
import com.kwabenaberko.weatheroo.weather.domain.model.CurrentWeather
import com.kwabenaberko.weatheroo.weather.domain.model.ForecastWeather
import com.kwabenaberko.weatheroo.weather.factory.WeatherTestFactory
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.verify
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class DbWeatherToDomainMapperTest{

    private val mockMapDbCurrentWeatherToDomain = mockk<(DbCurrentWeather) -> CurrentWeather>(relaxed = true)
    private val mockMapDbForecastWeatherToDomain = mockk<(DbForecastWeather) -> ForecastWeather>(relaxed = true)

    @Before
    fun setup(){
        coEvery { mockMapDbCurrentWeatherToDomain(any()) }.returns(WeatherTestFactory.makeCurrentWeather())
        coEvery { mockMapDbForecastWeatherToDomain(any()) }.returns(WeatherTestFactory.makeForecastWeather())
    }

    @Test
    fun `should map database weather to domain weather`() {

        val dbLocation = DbLocation(id = 1234, name = "Accra", country = "GH", lastUpdatedAt = 0L)
        val dbCurrentWeather = WeatherTestFactory.makeDbCurrentWeather()
        val dbForecastWeather = listOf(
            WeatherTestFactory.makeDbForecastWeather(),
            WeatherTestFactory.makeDbForecastWeather()
        )


        val domainWeather = DbWeatherToDomainMapper.map(
            dbLocation = dbLocation,
            dbCurrentWeather = dbCurrentWeather,
            dbForecastWeather = dbForecastWeather,
            mapDbCurrentWeatherToDomain = mockMapDbCurrentWeatherToDomain,
            mapDbForecastWeatherToDomain = mockMapDbForecastWeatherToDomain
        )


        assertEquals(dbLocation.name, domainWeather.location.name)
        assertEquals(dbLocation.country, domainWeather.location.country)
        verify { mockMapDbCurrentWeatherToDomain(any()) }
        verify { mockMapDbForecastWeatherToDomain(any()) }
    }

}