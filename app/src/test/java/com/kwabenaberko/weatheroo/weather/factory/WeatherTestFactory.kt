package com.kwabenaberko.weatheroo.weather.factory

import com.kwabenaberko.weatheroo.weather.data.db.model.DbCurrentWeather
import com.kwabenaberko.weatheroo.weather.data.db.model.DbForecastWeather
import com.kwabenaberko.weatheroo.weather.data.network.model.NetworkCurrentWeather
import com.kwabenaberko.weatheroo.weather.data.network.model.NetworkForecastWeather
import com.kwabenaberko.weatheroo.weather.data.network.model.NetworkWeather
import com.kwabenaberko.weatheroo.weather.domain.model.*
import com.kwabenaberko.weatheroo.weather.presentation.model.CurrentWeatherUiModel
import com.kwabenaberko.weatheroo.weather.presentation.model.ForecastWeatherUiModel
import org.threeten.bp.ZonedDateTime

object WeatherTestFactory {

    fun makeCurrentWeatherUiModel(
        icon: WeatherIcon = WeatherIcon.CLEAR_SKY_DAY,
        temperature: Int = 0,
        minTemperature: Int = 0,
        maxTemperature: Int = 0,
        condition: String = ""
    ): CurrentWeatherUiModel {
        return CurrentWeatherUiModel(icon, temperature, minTemperature, maxTemperature, condition)
    }

    fun makeForecastWeatherUiModel(
        formattedDate: String = "",
        icon: WeatherIcon = WeatherIcon.CLEAR_SKY_DAY,
        temperature: Int = 0,
        humidity: Int = 0,
        condition: String = "",
        chanceOfPrecipitationInPercentage: Int = 0
    ): ForecastWeatherUiModel {
        return ForecastWeatherUiModel(
            formattedDate,
            icon,
            temperature,
            humidity,
            condition,
            chanceOfPrecipitationInPercentage
        )
    }

    fun makeWeather(
        location: Location = LocationTestFactory.makeLocation(),
        current: CurrentWeather = makeCurrentWeather(),
        forecast: List<ForecastWeather> = listOf(makeForecastWeather())
    ): Weather {
        return Weather(location, current, forecast)
    }

    fun makeCurrentWeather(
        date: ZonedDateTime = ZonedDateTime.now(),
        icon: WeatherIcon = WeatherIcon.CLEAR_SKY_DAY,
        temperature: Double = 0.0,
        minTemperature: Double = 0.0,
        maxTemperature: Double = 0.0,
        condition: String = ""
    ): CurrentWeather {
        return CurrentWeather(date, icon, temperature, minTemperature, maxTemperature, condition)
    }

    fun makeForecastWeather(
        date: ZonedDateTime = ZonedDateTime.now(),
        icon: WeatherIcon = WeatherIcon.CLEAR_SKY_DAY,
        temperature: Double = 0.0,
        humidity: Double = 0.0,
        condition: String = "",
        chanceOfPrecipitation: Double = 0.0
    ): ForecastWeather {
        return ForecastWeather(date, icon, temperature, humidity, condition, chanceOfPrecipitation)
    }

    fun makeDbCurrentWeather(
        id: Int = 0,
        cityId: Int = 0,
        icon: String = "",
        temperature: Double = 0.0,
        minTemperature: Double = 0.0,
        maxTemperature: Double = 0.0,
        condition: String = "",
        date: Long = 0L
    ): DbCurrentWeather {
        return DbCurrentWeather(
            id,
            cityId,
            icon,
            temperature,
            minTemperature,
            maxTemperature,
            condition,
            date
        )
    }

    fun makeDbForecastWeather(
        id: Int = 0,
        cityId: Int = 0,
        icon: String = "",
        temperature: Double = 0.0,
        humidity: Double = 0.0,
        condition: String = "",
        precipitation: Double = 0.0,
        date: Long = 0L
    ): DbForecastWeather {
        return DbForecastWeather(
            id,
            cityId,
            icon,
            temperature,
            humidity,
            condition,
            precipitation,
            date
        )
    }


    fun makeNetworkWeather(
        main: String = "",
        icon: String = ""
    ): NetworkWeather {
        return NetworkWeather(main, icon)
    }


    fun makeNetworkCurrentWeather(
        id: Int = 0,
        name: String = "",
        dt: Long = 0L,
        weather: List<NetworkWeather> = listOf(makeNetworkWeather()),
        main: NetworkCurrentWeather.Main = NetworkCurrentWeather.Main(
            temp = 0.0,
            tempMin = 0.0,
            tempMax = 0.0
        ),
        sys: NetworkCurrentWeather.Sys = NetworkCurrentWeather.Sys(country = "")
    ): NetworkCurrentWeather {
        return NetworkCurrentWeather(id, name, dt, weather, main, sys)
    }


    fun makeNetworkForecastWeather(
        dt: Long = 0L,
        temp: NetworkForecastWeather.Temp = NetworkForecastWeather.Temp(max = 0.0),
        humidity: Double = 0.0,
        pop: Double = 0.0,
        weather: List<NetworkWeather> = listOf(makeNetworkWeather()),
    ): NetworkForecastWeather {
        return NetworkForecastWeather(dt, temp, humidity, pop, weather)
    }
}