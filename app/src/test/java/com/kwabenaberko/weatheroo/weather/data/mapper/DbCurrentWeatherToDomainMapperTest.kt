package com.kwabenaberko.weatheroo.weather.data.mapper

import com.kwabenaberko.weatheroo.weather.domain.model.WeatherIcon
import com.kwabenaberko.weatheroo.weather.factory.WeatherTestFactory
import org.junit.Assert.*
import org.junit.Test
import org.threeten.bp.*

class DbCurrentWeatherToDomainMapperTest {

    @Test
    fun `should map database current weather to domain current weather`() {

        val dbCurrentWeather = WeatherTestFactory.makeDbCurrentWeather(
            icon = "CLEAR_SKY_DAY",
            temperature = 29.0,
            minTemperature = 30.0,
            maxTemperature = 31.0,
            condition = "Clouds",
            date = 1618583905000
        )


        val domainCurrentWeather = DbCurrentWeatherToDomainMapper.map(dbCurrentWeather)

        assertEquals(WeatherIcon.CLEAR_SKY_DAY, domainCurrentWeather.icon)
        assertEquals(dbCurrentWeather.temperature, domainCurrentWeather.temperature, 0.0)
        assertEquals(dbCurrentWeather.minTemperature, domainCurrentWeather.minTemperature, 0.0)
        assertEquals(dbCurrentWeather.maxTemperature, domainCurrentWeather.maxTemperature, 0.0)
        assertEquals(dbCurrentWeather.condition, domainCurrentWeather.condition)

        val localDateTime = LocalDateTime.of(2021, Month.APRIL, 16, 14, 38, 25)
        assertEquals(ZonedDateTime.of(localDateTime, ZoneId.systemDefault()), domainCurrentWeather.date)
    }

}