package com.kwabenaberko.weatheroo.weather.data

import com.kwabenaberko.weatheroo.core.domain.model.Result
import com.kwabenaberko.weatheroo.weather.data.db.dao.LocationDao
import com.kwabenaberko.weatheroo.weather.data.db.dao.CurrentWeatherDao
import com.kwabenaberko.weatheroo.weather.data.db.dao.ForecastWeatherDao
import com.kwabenaberko.weatheroo.weather.data.db.model.DbLocation
import com.kwabenaberko.weatheroo.weather.data.db.model.DbCurrentWeather
import com.kwabenaberko.weatheroo.weather.data.db.model.DbForecastWeather
import com.kwabenaberko.weatheroo.weather.data.network.WeatherApi
import com.kwabenaberko.weatheroo.weather.data.network.model.FetchForecastWeatherResponse
import com.kwabenaberko.weatheroo.weather.data.network.model.NetworkCurrentWeather
import com.kwabenaberko.weatheroo.weather.data.network.model.NetworkForecastWeather
import com.kwabenaberko.weatheroo.weather.domain.WeatherRepository
import com.kwabenaberko.weatheroo.weather.domain.error.GetWeatherError
import com.kwabenaberko.weatheroo.weather.domain.model.Weather
import com.kwabenaberko.weatheroo.weather.factory.LocationTestFactory
import com.kwabenaberko.weatheroo.weather.factory.WeatherTestFactory
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody.Companion.toResponseBody
import okio.IOException
import org.hamcrest.CoreMatchers.instanceOf
import org.junit.Before

import org.junit.Assert.*
import org.junit.Test
import org.threeten.bp.ZonedDateTime
import retrofit2.Response

class WeatherRepositoryImplTest {

    private val mockWeatherApi = mockk<WeatherApi>()
    private val mockLocationDao = mockk<LocationDao>(relaxUnitFun = true)
    private val mockCurrentWeatherDao = mockk<CurrentWeatherDao>(relaxUnitFun = true)
    private val mockForecastWeatherDao = mockk<ForecastWeatherDao>(relaxUnitFun = true)
    private val mockMapNetworkCurrentWeatherToDb =
        mockk<(NetworkCurrentWeather, Int) -> DbCurrentWeather>()
    private val mockMapNetworkForecastWeatherToDb =
        mockk<(NetworkForecastWeather, Int) -> DbForecastWeather>()
    private val mockMapDbWeatherToDomain =
        mockk<(DbLocation, DbCurrentWeather, List<DbForecastWeather>) -> Weather>()

    private lateinit var repository: WeatherRepository

    @Before
    fun setUp() {
        repository = WeatherRepositoryImpl(
            weatherApi = mockWeatherApi,
            locationDao = mockLocationDao,
            currentWeatherDao = mockCurrentWeatherDao,
            forecastWeatherDao = mockForecastWeatherDao,
            mapNetworkCurrentWeatherToDb = mockMapNetworkCurrentWeatherToDb,
            mapNetworkForecastWeatherToDb = mockMapNetworkForecastWeatherToDb,
            mapDbWeatherToDomain = mockMapDbWeatherToDomain
        )
    }

    @Test
    fun `getWeatherForCity should call network api if a refresh is required`() = runBlocking {

        repository.getWeatherForCity(city = "", refresh = true)

        coVerify { mockWeatherApi.fetchCurrentWeatherForCity(any()) }
    }

    @Test
    fun `getWeatherForCity should call network api if location name does not exist in database`() =
        runBlocking {
            coEvery { mockLocationDao.hasLocation(any()) }.returns(false)

            repository.getWeatherForCity(city = "")

            coVerify { mockLocationDao.hasLocation(any()) }
            coVerify { mockWeatherApi.fetchCurrentWeatherForCity(any()) }
        }

    @Test
    fun `getWeatherForCity should call network api if location data has expired`() = runBlocking {
        val dbLocation = LocationTestFactory.makeDbLocation(
            lastUpdatedAt = ZonedDateTime.now().minusMinutes(6).toInstant().toEpochMilli()
        )

        coEvery { mockLocationDao.hasLocation(any()) }.returns(true)
        coEvery { mockLocationDao.findByName(any()) }
            .returns(dbLocation)

        repository.getWeatherForCity(city = "")

        coVerify { mockLocationDao.hasLocation(any()) }
        coVerify { mockLocationDao.findByName(any()) }
        coVerify { mockWeatherApi.fetchCurrentWeatherForCity(any()) }
    }

    @Test
    fun `getWeatherForCity should return network error if a network error occurs when fetching current weather`() =
        runBlocking {
            coEvery { mockWeatherApi.fetchCurrentWeatherForCity(any()) }
                .throws(IOException())

            val result = repository.getWeatherForCity(city = "", refresh = true)

            assertThat(result, instanceOf(Result.Fail::class.java))
            assertEquals(GetWeatherError.NetworkError, (result as Result.Fail).error)
            coVerify { mockWeatherApi.fetchCurrentWeatherForCity(any()) }
        }

    @Test
    fun `getWeatherForCity should return unknown error if an unexpected error occurs when fetching current weather`() =
        runBlocking {
            coEvery { mockWeatherApi.fetchCurrentWeatherForCity(any()) }
                .throws(Exception())

            val result = repository.getWeatherForCity(city = "", refresh = true)

            assertThat(result, instanceOf(Result.Fail::class.java))
            assertEquals(GetWeatherError.UnknownError, (result as Result.Fail).error)

        }


    @Test
    fun `getWeatherForCity should return location not found error if location does not exist when fetching current weather`() =
        runBlocking {
            coEvery { mockWeatherApi.fetchCurrentWeatherForCity(any()) }
                .returns(Response.error(404, "{}".toResponseBody()))

            val result = repository.getWeatherForCity(city = "", refresh = true)

            assertThat(result, instanceOf(Result.Fail::class.java))
            assertEquals(GetWeatherError.LocationNotFoundError, (result as Result.Fail).error)
            coVerify { mockWeatherApi.fetchCurrentWeatherForCity(any()) }
        }


    @Test
    fun `getWeatherForCity should return network error if a network error occurs when fetching forecast weather`() =
        runBlocking {

            coEvery { mockWeatherApi.fetchCurrentWeatherForCity(any()) }
                .returns(Response.success(WeatherTestFactory.makeNetworkCurrentWeather()))
            coEvery { mockWeatherApi.fetchForecastWeatherForCity(any()) }
                .throws(IOException())

            val result = repository.getWeatherForCity(city = "", refresh = true)

            assertThat(result, instanceOf(Result.Fail::class.java))
            assertEquals(GetWeatherError.NetworkError, (result as Result.Fail).error)
            coVerify { mockWeatherApi.fetchCurrentWeatherForCity(any()) }
            coVerify { mockWeatherApi.fetchForecastWeatherForCity(any()) }
        }

    @Test
    fun `getWeatherForCity should return unknown error if an unexpected error occurs when fetching forecast weather`() =
        runBlocking {
            coEvery { mockWeatherApi.fetchCurrentWeatherForCity(any()) }
                .returns(Response.success(WeatherTestFactory.makeNetworkCurrentWeather()))
            coEvery { mockWeatherApi.fetchForecastWeatherForCity(any()) }
                .throws(Exception())

            val result = repository.getWeatherForCity(city = "", refresh = true)

            assertThat(result, instanceOf(Result.Fail::class.java))
            assertEquals(GetWeatherError.UnknownError, (result as Result.Fail).error)
            coVerify { mockWeatherApi.fetchCurrentWeatherForCity(any()) }
            coVerify { mockWeatherApi.fetchForecastWeatherForCity(any()) }
        }

    @Test
    fun `getWeatherForCity should return location not found error if an unexpected error occurs when fetching forecast weather`() =
        runBlocking {

            coEvery { mockWeatherApi.fetchCurrentWeatherForCity(any()) }
                .returns(Response.success(WeatherTestFactory.makeNetworkCurrentWeather()))
            coEvery { mockWeatherApi.fetchForecastWeatherForCity(any()) }
                .returns(Response.error(404, "{}".toResponseBody()))

            val result = repository.getWeatherForCity(city = "", refresh = true)

            assertThat(result, instanceOf(Result.Fail::class.java))
            assertEquals(GetWeatherError.LocationNotFoundError, (result as Result.Fail).error)
            coVerify { mockWeatherApi.fetchCurrentWeatherForCity(any()) }
            coVerify { mockWeatherApi.fetchForecastWeatherForCity(any()) }

        }

    @Test
    fun `getWeatherForCity should return weather if weather data already exists in database and has not expired`() =
        runBlocking {
            val weather = WeatherTestFactory.makeWeather()
            coEvery { mockLocationDao.hasLocation(any()) }.returns(true)
            coEvery { mockLocationDao.findByName(any()) }
                .returns(
                    LocationTestFactory.makeDbLocation(
                        lastUpdatedAt = ZonedDateTime.now().toInstant().toEpochMilli()
                    )
                )
            coEvery { mockCurrentWeatherDao.findForLocation(any()) }
                .returns(WeatherTestFactory.makeDbCurrentWeather())
            coEvery { mockForecastWeatherDao.findForLocation(any()) }
                .returns(listOf(WeatherTestFactory.makeDbForecastWeather()))
            coEvery { mockMapDbWeatherToDomain(any(), any(), any()) }
                .returns(weather)

            val result = repository.getWeatherForCity(city = "")

            assertThat(result, instanceOf(Result.Ok::class.java))
            assertEquals(weather, (result as Result.Ok).data)
            coVerify { mockLocationDao.findByName(any()) }
            coVerify { mockCurrentWeatherDao.findForLocation(any()) }
            coVerify { mockForecastWeatherDao.findForLocation(any()) }
            verify { mockMapDbWeatherToDomain(any(), any(), any()) }

        }

    @Test
    fun `getWeatherForCity should return weather after calling network apis`() = runBlocking {
        val dbCurrentWeather = WeatherTestFactory.makeDbCurrentWeather()
        val dbForecastWeather = WeatherTestFactory.makeDbForecastWeather()
        val weather = WeatherTestFactory.makeWeather()


        coEvery { mockWeatherApi.fetchCurrentWeatherForCity(any()) }
            .returns(Response.success(WeatherTestFactory.makeNetworkCurrentWeather()))
        coEvery { mockWeatherApi.fetchForecastWeatherForCity(any()) }
            .returns(
                Response.success(
                    FetchForecastWeatherResponse(
                        list = listOf(
                            WeatherTestFactory.makeNetworkForecastWeather(),
                            WeatherTestFactory.makeNetworkForecastWeather()
                        )
                    )
                )
            )
        coEvery { mockLocationDao.findByName(any()) }.returns(LocationTestFactory.makeDbLocation())
        coEvery { mockCurrentWeatherDao.findForLocation(any()) }
            .returns(dbCurrentWeather)
        coEvery { mockForecastWeatherDao.findForLocation(any()) }
            .returns(listOf(dbForecastWeather))
        coEvery { mockMapNetworkCurrentWeatherToDb(any(), any()) }
            .returns(dbCurrentWeather)
        coEvery { mockMapNetworkForecastWeatherToDb(any(), any()) }
            .returns(dbForecastWeather)
        coEvery { mockMapDbWeatherToDomain(any(), any(), any()) }
            .returns(weather)


        val result = repository.getWeatherForCity(city = "", refresh = true)


        assertThat(result, instanceOf(Result.Ok::class.java))
        assertEquals(weather, (result as Result.Ok).data)
        coVerify { mockWeatherApi.fetchCurrentWeatherForCity(any()) }
        coVerify { mockWeatherApi.fetchForecastWeatherForCity(any()) }
        coVerify { mockLocationDao.deleteByName(any()) }
        coVerify { mockLocationDao.insertOrUpdate(any()) }
        coVerify { mockCurrentWeatherDao.insertOrUpdate(any()) }
        coVerify { mockForecastWeatherDao.insertOrUpdate(*anyVararg()) }
        coVerify { mockLocationDao.findByName(any()) }
        coVerify { mockCurrentWeatherDao.findForLocation(any()) }
        coVerify { mockForecastWeatherDao.findForLocation(any()) }
        verify { mockMapNetworkCurrentWeatherToDb(any(), any()) }
        verify { mockMapNetworkForecastWeatherToDb(any(), any()) }
        verify { mockMapDbWeatherToDomain(any(), any(), any()) }

    }
}















