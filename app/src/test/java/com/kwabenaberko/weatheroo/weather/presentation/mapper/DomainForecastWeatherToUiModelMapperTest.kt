package com.kwabenaberko.weatheroo.weather.presentation.mapper

import com.kwabenaberko.weatheroo.weather.domain.model.WeatherIcon
import com.kwabenaberko.weatheroo.weather.factory.WeatherTestFactory
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.threeten.bp.*

class DomainForecastWeatherToUiModelMapperTest {

    @Test
    fun `should map domain forecast to forecast ui model`() {
        val localDate = LocalDate.of(2021, Month.APRIL, 16,)

        val domainForecastWeather = WeatherTestFactory.makeForecastWeather(
            date = localDate.atStartOfDay(ZoneId.systemDefault()),
            icon = WeatherIcon.CLEAR_SKY_DAY,
            temperature = 25.0,
            humidity = 26.0,
            condition = "Clear Skies",
            chanceOfPrecipitation = 0.54
        )


        val forecastWeatherUiModel = DomainForecastWeatherToUiModelMapper.map(domainForecastWeather)

        assertEquals("Friday, 16 April, 2021", forecastWeatherUiModel.formattedDate)
        assertEquals(domainForecastWeather.icon, forecastWeatherUiModel.icon)
        assertEquals(domainForecastWeather.temperature.toInt(), forecastWeatherUiModel.temperature)
        assertEquals(domainForecastWeather.humidity.toInt(), forecastWeatherUiModel.humidity)
        assertEquals(domainForecastWeather.condition, forecastWeatherUiModel.condition)
        assertEquals(54, forecastWeatherUiModel.chanceOfPrecipitationInPercentage)

    }

}