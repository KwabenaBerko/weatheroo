package com.kwabenaberko.weatheroo.weather.presentation.mapper

import com.kwabenaberko.weatheroo.weather.domain.model.WeatherIcon
import com.kwabenaberko.weatheroo.weather.factory.WeatherTestFactory
import org.junit.Assert.*
import org.junit.Test
import org.threeten.bp.LocalDateTime
import org.threeten.bp.Month
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime

class DomainCurrentWeatherToUiModelMapperTest {

    @Test
    fun `should map domain current weather to current weather ui model`() {

        val currentWeather = WeatherTestFactory.makeCurrentWeather(
            date = ZonedDateTime.of(
                LocalDateTime.of(2021, Month.APRIL, 16, 22, 15),
                ZoneId.systemDefault()
            ),
            icon = WeatherIcon.CLEAR_SKY_DAY,
            temperature = 29.0,
            minTemperature = 30.0,
            maxTemperature = 32.0,
            condition = "Broken Clouds"
        )


        val currentWeatherUiModel = DomainCurrentWeatherToUiModelMapper.map(currentWeather)

        assertEquals(currentWeather.icon, currentWeatherUiModel.icon)
        assertEquals(currentWeather.temperature.toInt(), currentWeatherUiModel.temperature)
        assertEquals(currentWeather.minTemperature.toInt(), currentWeatherUiModel.minTemperature)
        assertEquals(currentWeather.maxTemperature.toInt(), currentWeatherUiModel.maxTemperature)
        assertEquals(currentWeather.condition, currentWeatherUiModel.condition)

    }

}