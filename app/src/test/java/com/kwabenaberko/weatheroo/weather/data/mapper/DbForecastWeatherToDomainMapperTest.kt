package com.kwabenaberko.weatheroo.weather.data.mapper

import com.kwabenaberko.weatheroo.weather.domain.model.WeatherIcon
import com.kwabenaberko.weatheroo.weather.factory.WeatherTestFactory
import org.junit.Assert.*
import org.junit.Test
import org.threeten.bp.LocalDateTime
import org.threeten.bp.Month
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime

class DbForecastWeatherToDomainMapperTest{

    @Test
    fun `should map database forecast weather to domain forecast weather`() {

        val dbForecastWeather = WeatherTestFactory.makeDbForecastWeather(
            icon = "CLEAR_SKY_NIGHT",
            temperature = 25.0,
            humidity = 53.0,
            condition = "Rain",
            precipitation = 0.32,
            date = 1618583905000
        )

        val domainForecastWeather = DbForecastWeatherToDomainMapper.map(dbForecastWeather)

        assertEquals(WeatherIcon.CLEAR_SKY_NIGHT, domainForecastWeather.icon)
        assertEquals(dbForecastWeather.temperature, domainForecastWeather.temperature, 0.0)
        assertEquals(dbForecastWeather.humidity, domainForecastWeather.humidity, 0.0)
        assertEquals(dbForecastWeather.condition, domainForecastWeather.condition)
        assertEquals(dbForecastWeather.precipitation, domainForecastWeather.chanceOfPrecipitation, 0.0)

        val localDateTime = LocalDateTime.of(2021, Month.APRIL, 16, 14, 38, 25)
        assertEquals(ZonedDateTime.of(localDateTime, ZoneId.systemDefault()), domainForecastWeather.date)

    }

}