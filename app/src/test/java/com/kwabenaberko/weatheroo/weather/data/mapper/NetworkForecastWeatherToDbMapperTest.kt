package com.kwabenaberko.weatheroo.weather.data.mapper

import com.kwabenaberko.weatheroo.weather.data.network.model.NetworkForecastWeather
import com.kwabenaberko.weatheroo.weather.data.network.model.NetworkWeather
import com.kwabenaberko.weatheroo.weather.domain.model.WeatherIcon
import com.kwabenaberko.weatheroo.weather.factory.WeatherTestFactory
import org.junit.Assert.assertEquals
import org.junit.Test

class NetworkForecastWeatherToDbMapperTest {

    @Test
    fun `should map network forecast weather to database forecast weather`() {

        val weatherId = 200
        val networkForecastWeather = WeatherTestFactory.makeNetworkForecastWeather(
            dt = 1618574400,
            temp = NetworkForecastWeather.Temp(max = 32.23),
            humidity = 57.0,
            pop = 0.74,
            weather = listOf(NetworkWeather(description = "Rain", icon = "10d"))
        )


        val dbForecastWeather = NetworkForecastWeatherToDbMapper.map(
            networkForecastWeather = networkForecastWeather,
            weatherId = weatherId
        )

        assertEquals(weatherId, dbForecastWeather.locationId)
        assertEquals((networkForecastWeather.dt * 1000), dbForecastWeather.date)
        assertEquals(networkForecastWeather.temp.max, dbForecastWeather.temperature, 0.0)
        assertEquals(networkForecastWeather.humidity, dbForecastWeather.humidity, 0.0)
        assertEquals(networkForecastWeather.pop, dbForecastWeather.precipitation, 0.0)
        assertEquals(networkForecastWeather.weather[0].description, dbForecastWeather.condition)
        assertEquals(WeatherIcon.RAIN_DAY.name, dbForecastWeather.icon)

    }

}