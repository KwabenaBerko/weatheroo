package com.kwabenaberko.weatheroo.weather.data.mapper

import com.kwabenaberko.weatheroo.weather.data.network.model.NetworkCurrentWeather
import com.kwabenaberko.weatheroo.weather.data.network.model.NetworkWeather
import com.kwabenaberko.weatheroo.weather.domain.model.WeatherIcon
import com.kwabenaberko.weatheroo.weather.factory.WeatherTestFactory
import org.junit.Assert.*
import org.junit.Test

class NetworkCurrentWeatherToDbMapperTest {

    @Test
    fun `should map network current weather to database current weather`() {

        val weatherId = 100
        val networkCurrentWeather = WeatherTestFactory.makeNetworkCurrentWeather(
            dt = 1618523995,
            name = "Accra",
            main = NetworkCurrentWeather.Main(
                temp = 27.0,
                tempMin = 27.0,
                tempMax = 27.0
            ),
            sys = NetworkCurrentWeather.Sys(country = "GH"),
            weather = listOf(NetworkWeather(description = "Clouds", icon = "02n"))
        )


        val dbCurrentWeather = NetworkCurrentWeatherToDbMapper.map(
            networkCurrentWeather = networkCurrentWeather,
            weatherId = weatherId
        )

        assertEquals(weatherId, dbCurrentWeather.locationId)
        assertEquals((networkCurrentWeather.dt * 1000), dbCurrentWeather.date)
        assertEquals(networkCurrentWeather.main.temp, dbCurrentWeather.temperature, 0.0)
        assertEquals(networkCurrentWeather.main.tempMin, dbCurrentWeather.minTemperature, 0.0)
        assertEquals(networkCurrentWeather.main.tempMax, dbCurrentWeather.maxTemperature, 0.0)
        assertEquals(networkCurrentWeather.weather[0].description, dbCurrentWeather.condition)
        assertEquals(WeatherIcon.FEW_CLOUDS_NIGHT.name, dbCurrentWeather.icon)

    }

}