package com.kwabenaberko.weatheroo.weather.factory

import com.kwabenaberko.weatheroo.weather.data.db.model.DbLocation
import com.kwabenaberko.weatheroo.weather.domain.model.Location

object LocationTestFactory {

    fun makeLocation(
        name: String = "",
        country: String = ""
    ): Location {
        return Location(name, country)
    }

    fun makeDbLocation(
        id: Int = 0,
        name: String = "",
        country: String = "",
        lastUpdatedAt: Long = 0L
    ): DbLocation{
        return DbLocation(id, name, country, lastUpdatedAt)
    }

}