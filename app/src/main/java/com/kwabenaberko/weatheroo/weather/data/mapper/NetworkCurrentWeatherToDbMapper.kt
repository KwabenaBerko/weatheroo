package com.kwabenaberko.weatheroo.weather.data.mapper

import com.kwabenaberko.weatheroo.core.capitalizeAll
import com.kwabenaberko.weatheroo.weather.data.db.model.DbCurrentWeather
import com.kwabenaberko.weatheroo.weather.data.getWeatherIcon
import com.kwabenaberko.weatheroo.weather.data.network.model.NetworkCurrentWeather
import java.util.*

object NetworkCurrentWeatherToDbMapper {

    fun map(networkCurrentWeather: NetworkCurrentWeather, weatherId: Int): DbCurrentWeather {
        return DbCurrentWeather(
            locationId = weatherId,
            icon = getWeatherIcon(networkCurrentWeather.weather[0].icon),
            temperature = networkCurrentWeather.main.temp,
            minTemperature = networkCurrentWeather.main.tempMin,
            maxTemperature = networkCurrentWeather.main.tempMax,
            condition = networkCurrentWeather.weather[0].description,
            date = networkCurrentWeather.dt * 1000 //Converting to milliseconds
        )
    }

}