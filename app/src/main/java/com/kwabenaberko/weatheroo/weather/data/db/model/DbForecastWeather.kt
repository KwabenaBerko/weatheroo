package com.kwabenaberko.weatheroo.weather.data.db.model

import androidx.room.*

@Entity(
    tableName = "forecast_weather",
    foreignKeys = [
        ForeignKey(
            entity = DbLocation::class,
            parentColumns = ["id"],
            childColumns = ["location_id"],
            onDelete = ForeignKey.CASCADE
        )
    ],
    indices = [Index("location_id")]
)
data class DbForecastWeather(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int = 0,

    @ColumnInfo(name = "location_id")
    val locationId: Int,

    @ColumnInfo(name = "icon")
    val icon: String,

    @ColumnInfo(name = "temperature")
    val temperature: Double,

    @ColumnInfo(name = "humidity")
    val humidity: Double,

    @ColumnInfo(name = "condition")
    val condition: String,

    @ColumnInfo(name = "precipitation")
    val precipitation: Double,

    @ColumnInfo(name = "date")
    val date: Long

)