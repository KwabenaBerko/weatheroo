package com.kwabenaberko.weatheroo.weather.domain.model

data class Location(
    val name: String,
    val country: String
)