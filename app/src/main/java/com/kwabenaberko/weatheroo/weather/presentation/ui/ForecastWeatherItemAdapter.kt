package com.kwabenaberko.weatheroo.weather.presentation.ui

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kwabenaberko.weatheroo.R
import com.kwabenaberko.weatheroo.databinding.ItemForecastWeatherBinding
import com.kwabenaberko.weatheroo.weather.presentation.getWeatherIconResource
import com.kwabenaberko.weatheroo.weather.presentation.model.ForecastWeatherUiModel

class ForecastWeatherItemAdapter(
    private val context: Context
) : RecyclerView.Adapter<ForecastWeatherItemAdapter.ForecastWeatherItemViewHolder>() {

    private val forecastWeatherList = mutableListOf<ForecastWeatherUiModel>()

    class ForecastWeatherItemViewHolder(
        private val binding: ItemForecastWeatherBinding
    ) : RecyclerView.ViewHolder(binding.root) {


        fun bind(
            context: Context,
            forecastWeather: ForecastWeatherUiModel
        ) {
            with(binding) {

                iconImageView.setImageResource(getWeatherIconResource(forecastWeather.icon))

                dateTextView.text = forecastWeather.formattedDate

                conditionsTextView.text = forecastWeather.condition

                humidityTextView.text = String.format(
                    context.getString(R.string.humidity),
                    forecastWeather.humidity
                )

                precipitationTextView.text = String.format(
                    context.getString(R.string.precipitation),
                    forecastWeather.chanceOfPrecipitationInPercentage
                )

                temperatureTextView.text = String.format(
                    context.getString(R.string.degree_fahrenheit),
                    forecastWeather.temperature
                )
            }
        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ForecastWeatherItemViewHolder {
        val binding = ItemForecastWeatherBinding.inflate(
            LayoutInflater.from(context),
            parent,
            false
        )

        return ForecastWeatherItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ForecastWeatherItemViewHolder, position: Int) {
        holder.bind(context, forecastWeatherList[position])
    }

    override fun getItemCount() = forecastWeatherList.size

    fun update(newList: List<ForecastWeatherUiModel>) {
        forecastWeatherList.clear()
        forecastWeatherList.addAll(newList)
        notifyDataSetChanged()
    }


}