package com.kwabenaberko.weatheroo.weather.presentation.model

import com.kwabenaberko.weatheroo.weather.domain.model.WeatherIcon

data class CurrentWeatherUiModel(
    val icon: WeatherIcon,

    val temperature: Int,

    val minTemperature: Int,

    val maxTemperature: Int,

    val condition: String
)