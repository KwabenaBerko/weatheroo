package com.kwabenaberko.weatheroo.weather.data.network.model

import com.squareup.moshi.Json

data class NetworkForecastWeather(

    @Json(name = "dt")
    val dt: Long,

    @Json(name = "temp")
    val temp: Temp,

    @Json(name = "humidity")
    val humidity: Double,

    @Json(name = "pop")
    val pop: Double,

    @Json(name = "weather")
    val weather: List<NetworkWeather>
){

    data class Temp(

        @Json(name = "max")
        val max: Double

    )

}