package com.kwabenaberko.weatheroo.weather.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kwabenaberko.weatheroo.weather.domain.model.CurrentWeather
import com.kwabenaberko.weatheroo.weather.domain.model.ForecastWeather
import com.kwabenaberko.weatheroo.weather.domain.usecase.GetWeatherForCityUseCase
import com.kwabenaberko.weatheroo.weather.presentation.model.CurrentWeatherUiModel
import com.kwabenaberko.weatheroo.weather.presentation.model.ForecastWeatherUiModel

@Suppress("UNCHECKED_CAST")
class WeatherViewModelFactory(
    private val getWeatherForCityUseCase: GetWeatherForCityUseCase,
    private val mapDomainCurrentWeatherToUiModel: (CurrentWeather) -> CurrentWeatherUiModel,
    private val mapDomainForecastWeatherToUiModel: (ForecastWeather) -> ForecastWeatherUiModel
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(WeatherViewModel::class.java)) {
            return WeatherViewModel(
                getWeatherForCityUseCase,
                mapDomainCurrentWeatherToUiModel,
                mapDomainForecastWeatherToUiModel
            ) as T
        }

        throw IllegalArgumentException()
    }
}