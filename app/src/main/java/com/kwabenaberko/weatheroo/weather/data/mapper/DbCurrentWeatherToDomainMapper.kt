package com.kwabenaberko.weatheroo.weather.data.mapper

import com.kwabenaberko.weatheroo.core.capitalizeAll
import com.kwabenaberko.weatheroo.weather.data.db.model.DbCurrentWeather
import com.kwabenaberko.weatheroo.weather.domain.model.CurrentWeather
import com.kwabenaberko.weatheroo.weather.domain.model.WeatherIcon
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime

object DbCurrentWeatherToDomainMapper {

    fun map(dbCurrentWeather: DbCurrentWeather): CurrentWeather{
        return CurrentWeather(
            date = ZonedDateTime.ofInstant(Instant.ofEpochMilli(dbCurrentWeather.date), ZoneId.systemDefault()),
            icon = WeatherIcon.valueOf(dbCurrentWeather.icon),
            temperature = dbCurrentWeather.temperature,
            minTemperature = dbCurrentWeather.minTemperature,
            maxTemperature = dbCurrentWeather.maxTemperature,
            condition = dbCurrentWeather.condition.capitalizeAll()
        )
    }

}