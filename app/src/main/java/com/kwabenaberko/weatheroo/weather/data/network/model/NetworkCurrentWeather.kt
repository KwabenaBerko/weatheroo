package com.kwabenaberko.weatheroo.weather.data.network.model

import com.squareup.moshi.Json

data class NetworkCurrentWeather(

    @Json(name = "id")
    val id: Int,

    @Json(name = "name")
    val name: String,

    @Json(name = "dt")
    val dt: Long,

    @Json(name = "weather")
    val weather: List<NetworkWeather>,

    @Json(name = "main")
    val main: Main,

    @Json(name = "sys")
    val sys: Sys
){

    data class Sys(val country: String)

    data class Main(
        @Json(name = "temp")
        val temp: Double,

        @Json(name = "temp_min")
        val tempMin: Double,

        @Json(name = "temp_max")
        val tempMax: Double
    )

}
