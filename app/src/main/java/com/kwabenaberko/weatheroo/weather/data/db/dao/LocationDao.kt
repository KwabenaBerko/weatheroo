package com.kwabenaberko.weatheroo.weather.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.kwabenaberko.weatheroo.core.data.db.BaseDao
import com.kwabenaberko.weatheroo.weather.data.db.model.DbLocation

@Dao
abstract class LocationDao : BaseDao<DbLocation>() {

    @Query(value = "SELECT EXISTS(SELECT * FROM locations WHERE name = :name COLLATE NOCASE)")
    abstract suspend fun hasLocation(name: String): Boolean

    @Query(value = "SELECT * FROM locations WHERE name = :name COLLATE NOCASE")
    abstract suspend fun findByName(name: String): DbLocation

    @Query(value = "DELETE FROM locations WHERE name = :name COLLATE NOCASE")
    abstract suspend fun deleteByName(name: String)

}