package com.kwabenaberko.weatheroo.weather.presentation

import android.content.Context
import androidx.annotation.IntegerRes
import androidx.core.content.ContextCompat
import com.kwabenaberko.weatheroo.R
import com.kwabenaberko.weatheroo.weather.domain.model.WeatherIcon

fun getWeatherIconResource(icon: WeatherIcon): Int {
    return when (icon) {
        WeatherIcon.CLEAR_SKY_DAY -> R.drawable.ic_clear_sky_day
        WeatherIcon.CLEAR_SKY_NIGHT -> R.drawable.ic_clear_sky_night
        WeatherIcon.FEW_CLOUDS_DAY -> R.drawable.ic_few_clouds_day
        WeatherIcon.FEW_CLOUDS_NIGHT -> R.drawable.ic_few_clouds_night
        WeatherIcon.SCATTERED_CLOUDS_DAY,
        WeatherIcon.SCATTERED_CLOUDS_NIGHT -> R.drawable.ic_scattered_clouds
        WeatherIcon.BROKEN_CLOUDS_DAY,
        WeatherIcon.BROKEN_CLOUDS_NIGHT -> R.drawable.ic_broken_clouds
        WeatherIcon.SHOWER_RAIN_DAY,
        WeatherIcon.SHOWER_RAIN_NIGHT -> R.drawable.ic_shower_rain
        WeatherIcon.RAIN_DAY -> R.drawable.ic_rain_day
        WeatherIcon.RAIN_NIGHT -> R.drawable.ic_rain_night
        WeatherIcon.THUNDERSTORM_DAY,
        WeatherIcon.THUNDERSTORM_NIGHT -> R.drawable.ic_thunderstorm
        WeatherIcon.SNOW_DAY,
        WeatherIcon.SNOW_NIGHT -> R.drawable.ic_snow
        WeatherIcon.MIST_DAY,
        WeatherIcon.MIST_NIGHT -> R.drawable.ic_mist
    }
}