package com.kwabenaberko.weatheroo.weather.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.kwabenaberko.weatheroo.core.data.db.BaseDao
import com.kwabenaberko.weatheroo.weather.data.db.model.DbCurrentWeather
import com.kwabenaberko.weatheroo.weather.domain.model.CurrentWeather

@Dao
abstract class CurrentWeatherDao: BaseDao<DbCurrentWeather>() {

    @Query(value = "SELECT * FROM current_weather WHERE location_id = :locationId")
    abstract suspend fun findForLocation(locationId: Int): DbCurrentWeather

}