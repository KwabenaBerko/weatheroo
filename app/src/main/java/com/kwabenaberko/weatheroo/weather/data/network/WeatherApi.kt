package com.kwabenaberko.weatheroo.weather.data.network

import com.kwabenaberko.weatheroo.BuildConfig
import com.kwabenaberko.weatheroo.weather.data.network.model.FetchForecastWeatherResponse
import com.kwabenaberko.weatheroo.weather.data.network.model.NetworkCurrentWeather
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {


    @GET(value = "/data/2.5/weather?units=imperial&appid=${BuildConfig.OPEN_WEATHER_MAP_API_KEY}")
    suspend fun fetchCurrentWeatherForCity(
        @Query(value = "q") city: String
    ): Response<NetworkCurrentWeather>

    @GET(value = "/data/2.5/forecast/daily?units=imperial&cnt=7&appid=${BuildConfig.OPEN_WEATHER_MAP_API_KEY}")
    suspend fun fetchForecastWeatherForCity(
        @Query(value = "q") city: String
    ): Response<FetchForecastWeatherResponse>

}