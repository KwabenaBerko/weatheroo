package com.kwabenaberko.weatheroo.weather.data.mapper

import com.kwabenaberko.weatheroo.core.capitalizeAll
import com.kwabenaberko.weatheroo.weather.data.db.model.DbForecastWeather
import com.kwabenaberko.weatheroo.weather.domain.model.ForecastWeather
import com.kwabenaberko.weatheroo.weather.domain.model.WeatherIcon
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime

object DbForecastWeatherToDomainMapper {

    fun map(dbForecastWeather: DbForecastWeather): ForecastWeather {
        return ForecastWeather(
            date = ZonedDateTime.ofInstant(
                Instant.ofEpochMilli(dbForecastWeather.date),
                ZoneId.systemDefault()
            ),
            icon = WeatherIcon.valueOf(dbForecastWeather.icon),
            temperature = dbForecastWeather.temperature,
            humidity = dbForecastWeather.humidity,
            condition = dbForecastWeather.condition.capitalizeAll(),
            chanceOfPrecipitation = dbForecastWeather.precipitation
        )
    }

}