package com.kwabenaberko.weatheroo.weather.presentation.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.kwabenaberko.weatheroo.R
import com.kwabenaberko.weatheroo.databinding.FragmentForecastWeatherBinding
import com.kwabenaberko.weatheroo.weather.domain.model.WeatherIcon
import com.kwabenaberko.weatheroo.weather.presentation.WeatherViewModel
import com.kwabenaberko.weatheroo.weather.presentation.WeatherViewState
import com.kwabenaberko.weatheroo.weather.presentation.model.ForecastWeatherUiModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForecastWeatherFragment : Fragment() {

    private var _binding: FragmentForecastWeatherBinding? = null
    private val binding get() = _binding!!
    private lateinit var forecastItemAdapter: ForecastWeatherItemAdapter
    private val viewModel: WeatherViewModel by viewModels({ requireActivity() })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        forecastItemAdapter = ForecastWeatherItemAdapter(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentForecastWeatherBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            forecastWeatherRecyclerView.apply {
                layoutManager = LinearLayoutManager(requireContext())
                adapter = forecastItemAdapter
            }
        }


        setupViewStateObserver()

    }

    private fun setupViewStateObserver() {
        viewModel.viewState.observe(viewLifecycleOwner, { viewState ->
            if (viewState is WeatherViewState.Content) {
                if (viewState.forecastWeatherList.isNotEmpty()) {
                    forecastItemAdapter.update(viewState.forecastWeatherList)
                }
            }
        })
    }
}