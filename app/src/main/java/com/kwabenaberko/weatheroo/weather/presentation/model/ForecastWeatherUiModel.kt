package com.kwabenaberko.weatheroo.weather.presentation.model

import com.kwabenaberko.weatheroo.weather.domain.model.WeatherIcon

data class ForecastWeatherUiModel(

    val formattedDate: String,

    val icon: WeatherIcon,

    val temperature: Int,

    val humidity: Int,

    val condition: String,

    val chanceOfPrecipitationInPercentage: Int
)