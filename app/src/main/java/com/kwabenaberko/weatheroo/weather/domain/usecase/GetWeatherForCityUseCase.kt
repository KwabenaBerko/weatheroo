package com.kwabenaberko.weatheroo.weather.domain.usecase

import com.kwabenaberko.weatheroo.core.domain.model.Result
import com.kwabenaberko.weatheroo.weather.domain.error.GetWeatherError
import com.kwabenaberko.weatheroo.weather.domain.model.Weather


/**
 * Using a functional useCase here(https://medium.com/swlh/functional-use-cases-f896f92e768f)
 * to reduce boilerplate code, as all it does is to forward the call to the repository
 */
typealias GetWeatherForCityUseCase = suspend (String, Boolean) -> Result<GetWeatherError, Weather>