package com.kwabenaberko.weatheroo.weather.domain.model

import org.threeten.bp.ZonedDateTime

data class CurrentWeather(

    val date: ZonedDateTime,

    val icon: WeatherIcon,

    val temperature: Double,

    val minTemperature: Double,

    val maxTemperature: Double,

    val condition: String,
)