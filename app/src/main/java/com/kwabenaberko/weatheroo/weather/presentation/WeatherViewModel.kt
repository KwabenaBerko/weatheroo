package com.kwabenaberko.weatheroo.weather.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kwabenaberko.weatheroo.core.domain.model.Result
import com.kwabenaberko.weatheroo.weather.domain.model.CurrentWeather
import com.kwabenaberko.weatheroo.weather.domain.model.ForecastWeather
import com.kwabenaberko.weatheroo.weather.domain.usecase.GetWeatherForCityUseCase
import com.kwabenaberko.weatheroo.weather.presentation.model.CurrentWeatherUiModel
import com.kwabenaberko.weatheroo.weather.presentation.model.ForecastWeatherUiModel
import kotlinx.coroutines.launch
import java.util.*

class WeatherViewModel(
    private val getWeatherForCityUseCase: GetWeatherForCityUseCase,
    private val mapDomainCurrentWeatherToUiModel: (CurrentWeather) -> CurrentWeatherUiModel,
    private val mapDomainForecastWeatherToUiModel: (ForecastWeather) -> ForecastWeatherUiModel
) : ViewModel() {

    private var lastSearchedCity: String = ""
    private val _viewState = MutableLiveData<WeatherViewState>(WeatherViewState.Initial)
    val viewState: LiveData<WeatherViewState> = _viewState

    fun getWeatherForCity(city: String) {
        loadWeather(city)
    }

    fun refreshWeather() {
        loadWeather(city = lastSearchedCity, refresh = true)
    }

    private fun loadWeather(city: String, refresh: Boolean = false) {
        viewModelScope.launch {

            if (refresh) {
                val contentState = viewState.value as WeatherViewState.Content
                _viewState.value = contentState.copy(isRefreshing = true)
            } else {
                _viewState.value = WeatherViewState.Loading
            }

            when (val result = getWeatherForCityUseCase(city.toLowerCase(Locale.ROOT), refresh)) {
                is Result.Fail -> {
                    _viewState.value = WeatherViewState.Failed(result.error)
                }
                is Result.Ok -> {
                    lastSearchedCity = city

                    val weather = result.data

                    _viewState.value = WeatherViewState.Content(
                        location = "${weather.location.name}, ${weather.location.country}",
                        currentWeather = mapDomainCurrentWeatherToUiModel(weather.current),
                        forecastWeatherList = weather.forecast.map(mapDomainForecastWeatherToUiModel)
                    )
                }
            }
        }
    }



}