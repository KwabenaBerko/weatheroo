package com.kwabenaberko.weatheroo.weather.presentation

import com.kwabenaberko.weatheroo.weather.domain.error.GetWeatherError
import com.kwabenaberko.weatheroo.weather.domain.model.Location
import com.kwabenaberko.weatheroo.weather.presentation.model.CurrentWeatherUiModel
import com.kwabenaberko.weatheroo.weather.presentation.model.ForecastWeatherUiModel

sealed class WeatherViewState {

    object Initial : WeatherViewState()

    object Loading : WeatherViewState()

    data class Content(

        val isRefreshing: Boolean = false,

        val location: String,

        val currentWeather: CurrentWeatherUiModel,

        val forecastWeatherList: List<ForecastWeatherUiModel>

    ) : WeatherViewState()

    data class Failed(val error: GetWeatherError) : WeatherViewState()

}