package com.kwabenaberko.weatheroo.weather.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.kwabenaberko.weatheroo.core.data.db.BaseDao
import com.kwabenaberko.weatheroo.weather.data.db.model.DbForecastWeather

@Dao
abstract class ForecastWeatherDao : BaseDao<DbForecastWeather>() {

    @Query(value = "SELECT * FROM forecast_weather WHERE location_id = :locationId")
    abstract suspend fun findForLocation(locationId: Int): List<DbForecastWeather>

}