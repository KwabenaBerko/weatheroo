package com.kwabenaberko.weatheroo.weather.domain.error

sealed class GetWeatherError {

    object LocationNotFoundError : GetWeatherError()

    object NetworkError : GetWeatherError()

    object UnknownError : GetWeatherError()
}
