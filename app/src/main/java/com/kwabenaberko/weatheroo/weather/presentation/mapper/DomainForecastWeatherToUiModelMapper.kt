package com.kwabenaberko.weatheroo.weather.presentation.mapper

import com.kwabenaberko.weatheroo.core.Constants
import com.kwabenaberko.weatheroo.weather.domain.model.ForecastWeather
import com.kwabenaberko.weatheroo.weather.presentation.model.ForecastWeatherUiModel
import org.threeten.bp.format.DateTimeFormatter

object DomainForecastWeatherToUiModelMapper {

    fun map(forecastWeather: ForecastWeather): ForecastWeatherUiModel {
        return ForecastWeatherUiModel(
            formattedDate = forecastWeather.date.format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT_PATTERN)),
            icon = forecastWeather.icon,
            temperature = forecastWeather.temperature.toInt(),
            humidity = forecastWeather.humidity.toInt(),
            condition = forecastWeather.condition,
            chanceOfPrecipitationInPercentage = (forecastWeather.chanceOfPrecipitation * 100).toInt()
        )
    }

}