package com.kwabenaberko.weatheroo.weather.data.db.model

import androidx.room.*

@Entity(
    tableName = "current_weather",
    foreignKeys = [
        ForeignKey(
            entity = DbLocation::class,
            parentColumns = ["id"],
            childColumns = ["location_id"],
            onDelete = ForeignKey.CASCADE
        )
    ],
    indices = [Index("location_id")]
)
data class DbCurrentWeather(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int = 0,

    @ColumnInfo(name = "location_id")
    val locationId: Int,

    @ColumnInfo(name = "icon")
    val icon: String,

    @ColumnInfo(name = "temperature")
    val temperature: Double,

    @ColumnInfo(name = "min_temperature")
    val minTemperature: Double,

    @ColumnInfo(name = "max_temperature")
    val maxTemperature: Double,

    @ColumnInfo(name = "condition")
    val condition: String,

    @ColumnInfo(name = "date")
    val date: Long
)