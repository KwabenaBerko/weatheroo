package com.kwabenaberko.weatheroo.weather.presentation.ui

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.doAfterTextChanged
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.kwabenaberko.weatheroo.R
import com.kwabenaberko.weatheroo.databinding.ActivityWeatherBinding
import com.kwabenaberko.weatheroo.weather.domain.error.GetWeatherError
import com.kwabenaberko.weatheroo.weather.presentation.WeatherViewModel
import com.kwabenaberko.weatheroo.weather.presentation.WeatherViewModelFactory
import com.kwabenaberko.weatheroo.weather.presentation.WeatherViewState
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class WeatherActivity : AppCompatActivity() {

    @Inject
    lateinit var weatherViewModelFactory: WeatherViewModelFactory

    private var _binding: ActivityWeatherBinding? = null
    private val binding get() = _binding!!
    private val viewModel: WeatherViewModel by viewModels { weatherViewModelFactory }

    private object ViewFlipper {
        const val INITIAL = 0
        const val LOADING = 1
        const val DATA = 2
        const val ERROR = 3
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityWeatherBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.navHostFragment)
        val navController = (navHostFragment as NavHostFragment).navController


        with(binding) {
            bottomNavigationView.setupWithNavController(navController)
            bottomNavigationView.setOnNavigationItemReselectedListener { }

            searchCityEditText.doAfterTextChanged {
                goBtn.isEnabled = it.toString().isNotEmpty()
            }

            goBtn.setOnClickListener {
                hideKeyboard(currentFocus)
                viewModel.getWeatherForCity(
                    city = searchCityEditText.text.toString()
                )
            }

            swipeRefreshLayout.setOnRefreshListener {
                viewModel.refreshWeather()
            }
        }

        setupViewStateObserver()

    }

    private fun setupViewStateObserver() {
        viewModel.viewState.observe(this) { viewState ->
            with(binding) {
                if (viewState is WeatherViewState.Content) {
                    swipeRefreshLayout.visibility = View.VISIBLE
                } else {
                    swipeRefreshLayout.visibility = View.GONE
                }

                when (viewState) {
                    is WeatherViewState.Initial -> {
                        viewFlipper.displayedChild = ViewFlipper.INITIAL
                    }
                    is WeatherViewState.Loading -> {
                        viewFlipper.displayedChild = ViewFlipper.LOADING
                    }
                    is WeatherViewState.Content -> {
                        viewFlipper.displayedChild = ViewFlipper.DATA

                        swipeRefreshLayout.isRefreshing = viewState.isRefreshing

                        contentLayout.setBackgroundColor(
                            ContextCompat.getColor(
                                this@WeatherActivity,
                                if (isNight()) R.color.night
                                else R.color.day
                            )
                        )
                    }
                    is WeatherViewState.Failed -> {

                        viewFlipper.displayedChild = ViewFlipper.ERROR

                        errorLayout.errorLabelTextView.text = when (viewState.error) {
                            is GetWeatherError.NetworkError -> getString(R.string.no_network)
                            is GetWeatherError.LocationNotFoundError -> getString(R.string.location_not_found)
                            is GetWeatherError.UnknownError -> getString(R.string.unexpected_error)
                        }

                        errorLayout.errorIconImageView.setImageResource(
                            when (viewState.error) {
                                is GetWeatherError.NetworkError -> R.drawable.ic_connection
                                else -> R.drawable.ic_emoji_confused
                            }
                        )
                    }
                }
            }
        }
    }


    private fun hideKeyboard(currentFocus: View?) {
        if (currentFocus != null) {
            (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                .hideSoftInputFromWindow(currentFocus.windowToken, 0)
        }
    }

    private fun isNight(): Boolean {
        val hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
        return hour < 6 || hour > 18
    }

}