package com.kwabenaberko.weatheroo.weather.presentation.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.kwabenaberko.weatheroo.R
import com.kwabenaberko.weatheroo.databinding.FragmentCurrentWeatherBinding
import com.kwabenaberko.weatheroo.weather.presentation.WeatherViewModel
import com.kwabenaberko.weatheroo.weather.presentation.WeatherViewState
import com.kwabenaberko.weatheroo.weather.presentation.getWeatherIconResource
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class CurrentWeatherFragment : Fragment() {

    private var _binding: FragmentCurrentWeatherBinding? = null
    private val binding get() = _binding!!
    private val viewModel: WeatherViewModel by viewModels({ requireActivity() })

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCurrentWeatherBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewStateObserver()
    }

    private fun setupViewStateObserver() {
        viewModel.viewState.observe(viewLifecycleOwner, { viewState ->
            if (viewState is WeatherViewState.Content) {

                val (icon, temperature, minTemperature, maxTemperature, condition) = viewState.currentWeather

                with(binding) {

                    iconImageView.setImageResource(getWeatherIconResource(icon))

                    temperatureTextView.text = String.format(
                        getString(R.string.degree_fahrenheit),
                        temperature
                    )

                    locationTextView.text = viewState.location
                }
            }
        })
    }

}