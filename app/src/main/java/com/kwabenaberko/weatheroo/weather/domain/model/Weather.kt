package com.kwabenaberko.weatheroo.weather.domain.model

data class Weather(

    val location: Location,

    val current: CurrentWeather,

    val forecast: List<ForecastWeather>
)