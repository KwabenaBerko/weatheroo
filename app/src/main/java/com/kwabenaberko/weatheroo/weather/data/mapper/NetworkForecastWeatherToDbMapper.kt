package com.kwabenaberko.weatheroo.weather.data.mapper

import com.kwabenaberko.weatheroo.core.capitalizeAll
import com.kwabenaberko.weatheroo.weather.data.db.model.DbForecastWeather
import com.kwabenaberko.weatheroo.weather.data.getWeatherIcon
import com.kwabenaberko.weatheroo.weather.data.network.model.NetworkForecastWeather
import java.util.*

object NetworkForecastWeatherToDbMapper {

    fun map(networkForecastWeather: NetworkForecastWeather, weatherId: Int): DbForecastWeather {
        return DbForecastWeather(
            locationId = weatherId,
            icon = getWeatherIcon(networkForecastWeather.weather[0].icon),
            temperature = networkForecastWeather.temp.max,
            humidity = networkForecastWeather.humidity,
            condition = networkForecastWeather.weather[0].description,
            precipitation = networkForecastWeather.pop,
            date = networkForecastWeather.dt * 1000
        )
    }

}