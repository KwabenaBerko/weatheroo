package com.kwabenaberko.weatheroo.weather.data

import com.kwabenaberko.weatheroo.weather.domain.model.WeatherIcon



/**
 * To decouple the icons from the weather api service in use at any point in time,
 * in my opinion, its best to convert them to a format that makes sense to this app. In this particular case,
 * we convert them to an enum and save its string value. By doing this, we decouple the app's
 * icon names from that of the weather api service.
 */

fun getWeatherIcon(icon: String): String {
    val weatherIcon = when (icon) {
        "01n" -> WeatherIcon.CLEAR_SKY_NIGHT
        "02d" -> WeatherIcon.FEW_CLOUDS_DAY
        "02n" -> WeatherIcon.FEW_CLOUDS_NIGHT
        "03d" -> WeatherIcon.SCATTERED_CLOUDS_DAY
        "03n" -> WeatherIcon.SCATTERED_CLOUDS_NIGHT
        "04d" -> WeatherIcon.BROKEN_CLOUDS_DAY
        "04n" -> WeatherIcon.BROKEN_CLOUDS_NIGHT
        "09d" -> WeatherIcon.SHOWER_RAIN_DAY
        "09n" -> WeatherIcon.SHOWER_RAIN_NIGHT
        "10d" -> WeatherIcon.RAIN_DAY
        "10n" -> WeatherIcon.RAIN_NIGHT
        "11d" -> WeatherIcon.THUNDERSTORM_DAY
        "11n" -> WeatherIcon.THUNDERSTORM_NIGHT
        "13d" -> WeatherIcon.SNOW_DAY
        "13n" -> WeatherIcon.SNOW_NIGHT
        "50d" -> WeatherIcon.MIST_DAY
        "50n" -> WeatherIcon.MIST_NIGHT
        else -> WeatherIcon.CLEAR_SKY_DAY
    }

    return weatherIcon.name
}