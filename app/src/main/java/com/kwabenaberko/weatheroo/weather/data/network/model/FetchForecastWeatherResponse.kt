package com.kwabenaberko.weatheroo.weather.data.network.model

import com.squareup.moshi.Json

data class FetchForecastWeatherResponse(

    @Json(name = "list")
    val list: List<NetworkForecastWeather>
)