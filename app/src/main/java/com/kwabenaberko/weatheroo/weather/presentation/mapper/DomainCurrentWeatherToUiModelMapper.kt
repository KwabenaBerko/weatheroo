package com.kwabenaberko.weatheroo.weather.presentation.mapper

import com.kwabenaberko.weatheroo.weather.domain.model.CurrentWeather
import com.kwabenaberko.weatheroo.weather.presentation.model.CurrentWeatherUiModel

object DomainCurrentWeatherToUiModelMapper {

    fun map(currentWeather: CurrentWeather): CurrentWeatherUiModel{
        return CurrentWeatherUiModel(
            icon = currentWeather.icon,
            temperature = currentWeather.temperature.toInt(),
            minTemperature = currentWeather.minTemperature.toInt(),
            maxTemperature = currentWeather.maxTemperature.toInt(),
            condition = currentWeather.condition
        )
    }

}