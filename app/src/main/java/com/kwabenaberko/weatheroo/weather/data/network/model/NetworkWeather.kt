package com.kwabenaberko.weatheroo.weather.data.network.model

import com.squareup.moshi.Json

data class NetworkWeather(

    @Json(name = "description")
    val description: String,

    @Json(name = "icon")
    val icon: String

)

