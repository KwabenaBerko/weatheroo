package com.kwabenaberko.weatheroo.weather.data.mapper

import com.kwabenaberko.weatheroo.core.capitalizeAll
import com.kwabenaberko.weatheroo.weather.data.db.model.DbLocation
import com.kwabenaberko.weatheroo.weather.data.db.model.DbCurrentWeather
import com.kwabenaberko.weatheroo.weather.data.db.model.DbForecastWeather
import com.kwabenaberko.weatheroo.weather.domain.model.CurrentWeather
import com.kwabenaberko.weatheroo.weather.domain.model.ForecastWeather
import com.kwabenaberko.weatheroo.weather.domain.model.Location
import com.kwabenaberko.weatheroo.weather.domain.model.Weather
import java.util.*

object DbWeatherToDomainMapper {

    fun map(
        dbLocation: DbLocation,
        dbCurrentWeather: DbCurrentWeather,
        dbForecastWeather: List<DbForecastWeather>,
        mapDbCurrentWeatherToDomain: (DbCurrentWeather) -> CurrentWeather,
        mapDbForecastWeatherToDomain: (DbForecastWeather) -> ForecastWeather
    ): Weather {
        return Weather(
            location = Location(name = dbLocation.name.capitalizeAll(), country = dbLocation.country),
            current = mapDbCurrentWeatherToDomain(dbCurrentWeather),
            forecast = dbForecastWeather.map(mapDbForecastWeatherToDomain)
        )
    }

}