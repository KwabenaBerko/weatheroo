package com.kwabenaberko.weatheroo.weather.data

import com.kwabenaberko.weatheroo.core.domain.model.Result
import com.kwabenaberko.weatheroo.weather.data.db.dao.CurrentWeatherDao
import com.kwabenaberko.weatheroo.weather.data.db.dao.ForecastWeatherDao
import com.kwabenaberko.weatheroo.weather.data.db.dao.LocationDao
import com.kwabenaberko.weatheroo.weather.data.db.model.DbLocation
import com.kwabenaberko.weatheroo.weather.data.db.model.DbCurrentWeather
import com.kwabenaberko.weatheroo.weather.data.db.model.DbForecastWeather
import com.kwabenaberko.weatheroo.weather.data.network.WeatherApi
import com.kwabenaberko.weatheroo.weather.data.network.model.NetworkCurrentWeather
import com.kwabenaberko.weatheroo.weather.data.network.model.NetworkForecastWeather
import com.kwabenaberko.weatheroo.weather.domain.WeatherRepository
import com.kwabenaberko.weatheroo.weather.domain.error.GetWeatherError
import com.kwabenaberko.weatheroo.weather.domain.model.Weather
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okio.IOException
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime
import timber.log.Timber
import java.net.HttpURLConnection
import java.util.*
import java.util.concurrent.TimeUnit

class WeatherRepositoryImpl constructor(
    private val weatherApi: WeatherApi,
    private val locationDao: LocationDao,
    private val currentWeatherDao: CurrentWeatherDao,
    private val forecastWeatherDao: ForecastWeatherDao,
    private val mapNetworkCurrentWeatherToDb: (NetworkCurrentWeather, Int) -> DbCurrentWeather,
    private val mapNetworkForecastWeatherToDb: (NetworkForecastWeather, Int) -> DbForecastWeather,
    private val mapDbWeatherToDomain: (DbLocation, DbCurrentWeather, List<DbForecastWeather>) -> Weather
) : WeatherRepository {

    private companion object {
        const val LOCATION_DATA_VALID_FOR_MINUTES = 5
    }

    override suspend fun getWeatherForCity(
        city: String,
        refresh: Boolean
    ): Result<GetWeatherError, Weather> = withContext(Dispatchers.IO) {
        try {

            if (
                refresh ||
                !locationDao.hasLocation(city) ||
                hasLocationDataExpired(locationDao.findByName(city).lastUpdatedAt)
            ) {

                val currentWeatherResponse = weatherApi.fetchCurrentWeatherForCity(city)
                if (currentWeatherResponse.code() == HttpURLConnection.HTTP_NOT_FOUND) {
                    return@withContext Result.Fail(GetWeatherError.LocationNotFoundError)
                }

                val forecastWeatherResponse = weatherApi.fetchForecastWeatherForCity(city)
                if (forecastWeatherResponse.code() == HttpURLConnection.HTTP_NOT_FOUND) {
                    return@withContext Result.Fail(GetWeatherError.LocationNotFoundError)
                }

                val currentWeather = currentWeatherResponse.body()!!
                val forecastWeatherList = forecastWeatherResponse.body()!!.list

                locationDao.deleteByName(city)

                val dbLocation = DbLocation(
                    id = currentWeather.id,
                    name = city,
                    country = currentWeather.sys.country,
                    lastUpdatedAt = ZonedDateTime.now().toInstant().toEpochMilli()
                )

                locationDao.insertOrUpdate(dbLocation)
                currentWeatherDao.insertOrUpdate(
                    mapNetworkCurrentWeatherToDb(currentWeather, dbLocation.id)
                )
                forecastWeatherDao.insertOrUpdate(
                    *forecastWeatherList.map { forecastWeather ->
                        mapNetworkForecastWeatherToDb(
                            forecastWeather,
                            dbLocation.id
                        )
                    }.toTypedArray()
                )
            }


            val dbLocation = locationDao.findByName(city)
            val dbCurrentWeather = currentWeatherDao.findForLocation(dbLocation.id)
            val dbForecastWeatherList = forecastWeatherDao.findForLocation(dbLocation.id)

            val weather = mapDbWeatherToDomain(dbLocation, dbCurrentWeather, dbForecastWeatherList)
            Result.Ok(weather)

        } catch (exception: Exception) {
            Timber.e(exception)
            if (exception is IOException) {
                return@withContext Result.Fail(GetWeatherError.NetworkError)
            }
            Result.Fail(GetWeatherError.UnknownError)
        }
    }


    private fun hasLocationDataExpired(lastUpdatedAt: Long): Boolean {
        val duration = Duration.between(
            ZonedDateTime.ofInstant(Instant.ofEpochMilli(lastUpdatedAt), ZoneId.systemDefault()),
            ZonedDateTime.now()
        )


        return duration.toMinutes() > LOCATION_DATA_VALID_FOR_MINUTES
    }
}