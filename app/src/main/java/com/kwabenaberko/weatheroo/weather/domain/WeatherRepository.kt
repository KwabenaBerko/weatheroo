package com.kwabenaberko.weatheroo.weather.domain

import com.kwabenaberko.weatheroo.core.domain.model.Result
import com.kwabenaberko.weatheroo.weather.domain.error.GetWeatherError
import com.kwabenaberko.weatheroo.weather.domain.model.Weather

interface WeatherRepository {

    suspend fun getWeatherForCity(
        city: String,
        refresh: Boolean = false
    ): Result<GetWeatherError, Weather>

}