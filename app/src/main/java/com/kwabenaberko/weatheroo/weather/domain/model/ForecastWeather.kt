package com.kwabenaberko.weatheroo.weather.domain.model

import org.threeten.bp.ZonedDateTime

data class ForecastWeather(

    val date: ZonedDateTime,

    val icon: WeatherIcon,

    val temperature: Double,

    val humidity: Double,

    val condition: String,

    val chanceOfPrecipitation: Double
)