package com.kwabenaberko.weatheroo.core.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.kwabenaberko.weatheroo.weather.data.db.dao.CurrentWeatherDao
import com.kwabenaberko.weatheroo.weather.data.db.dao.ForecastWeatherDao
import com.kwabenaberko.weatheroo.weather.data.db.dao.LocationDao
import com.kwabenaberko.weatheroo.weather.data.db.model.DbCurrentWeather
import com.kwabenaberko.weatheroo.weather.data.db.model.DbForecastWeather
import com.kwabenaberko.weatheroo.weather.data.db.model.DbLocation


@Database(
    entities = [
        DbLocation::class,
        DbCurrentWeather::class,
        DbForecastWeather::class
    ],
    exportSchema = false,
    version = 1
)
abstract class WeatherooDatabase : RoomDatabase() {

    abstract fun locationDao(): LocationDao
    abstract fun currentWeatherDao(): CurrentWeatherDao
    abstract fun forecastWeatherDao(): ForecastWeatherDao

    companion object {
        private var INSTANCE: WeatherooDatabase? = null

        fun getInstance(context: Context): WeatherooDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    context,
                    WeatherooDatabase::class.java,
                    "weatheroo.db"
                ).build()
            }

            return INSTANCE as WeatherooDatabase
        }
    }

}