package com.kwabenaberko.weatheroo.core.di.module

import android.content.Context
import com.kwabenaberko.weatheroo.BuildConfig
import com.kwabenaberko.weatheroo.core.Constants
import com.kwabenaberko.weatheroo.core.data.db.WeatherooDatabase
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
abstract class CoreModule {

    companion object {


        @Singleton
        @Provides
        fun provideContext(@ApplicationContext context: Context): Context {
            return context
        }


        @Singleton
        @Provides
        fun provideMoshi(): Moshi {
            return Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()
        }

        @Singleton
        @Provides
        fun provideOkHttpLoggingInterceptor(): HttpLoggingInterceptor {
            return HttpLoggingInterceptor()
                .apply {
                    level =
                        if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
                        else HttpLoggingInterceptor.Level.NONE
                }
        }

        @Singleton
        @Provides
        fun provideOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
            return OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build()
        }

        @Singleton
        @Provides
        fun provideRetrofit(okHttpClient: OkHttpClient, moshi: Moshi): Retrofit {
            return Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .build()
        }


        @Singleton
        @Provides
        fun provideWeatherooDatabase(context: Context): WeatherooDatabase {
            return WeatherooDatabase.getInstance(context)
        }

    }

}