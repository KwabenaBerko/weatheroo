package com.kwabenaberko.weatheroo.core.di.module

import com.kwabenaberko.weatheroo.core.data.db.WeatherooDatabase
import com.kwabenaberko.weatheroo.weather.data.WeatherRepositoryImpl
import com.kwabenaberko.weatheroo.weather.data.mapper.*
import com.kwabenaberko.weatheroo.weather.data.network.WeatherApi
import com.kwabenaberko.weatheroo.weather.domain.WeatherRepository
import com.kwabenaberko.weatheroo.weather.presentation.WeatherViewModelFactory
import com.kwabenaberko.weatheroo.weather.presentation.mapper.DomainCurrentWeatherToUiModelMapper
import com.kwabenaberko.weatheroo.weather.presentation.mapper.DomainForecastWeatherToUiModelMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module(includes = [CoreModule::class])
abstract class WeatherModule {

    companion object {

        @Singleton
        @Provides
        fun provideWeatherRepository(
            retrofit: Retrofit,
            database: WeatherooDatabase
        ): WeatherRepository {
            return WeatherRepositoryImpl(
                weatherApi = retrofit.create(WeatherApi::class.java),
                locationDao = database.locationDao(),
                currentWeatherDao = database.currentWeatherDao(),
                forecastWeatherDao = database.forecastWeatherDao(),
                mapNetworkCurrentWeatherToDb = NetworkCurrentWeatherToDbMapper::map,
                mapNetworkForecastWeatherToDb = NetworkForecastWeatherToDbMapper::map,
                mapDbWeatherToDomain = { dbLocation, dbCurrentWeather, dbForecastWeatherList ->
                    DbWeatherToDomainMapper.map(
                        dbLocation = dbLocation,
                        dbCurrentWeather = dbCurrentWeather,
                        dbForecastWeather = dbForecastWeatherList,
                        mapDbCurrentWeatherToDomain = DbCurrentWeatherToDomainMapper::map,
                        mapDbForecastWeatherToDomain = DbForecastWeatherToDomainMapper::map
                    )
                }
            )
        }

        @Singleton
        @Provides
        fun provideWeatherViewModelFactory(
            weatherRepository: WeatherRepository
        ): WeatherViewModelFactory {
            return WeatherViewModelFactory(
                getWeatherForCityUseCase = weatherRepository::getWeatherForCity,
                mapDomainCurrentWeatherToUiModel = DomainCurrentWeatherToUiModelMapper::map,
                mapDomainForecastWeatherToUiModel = DomainForecastWeatherToUiModelMapper::map
            )
        }

    }

}