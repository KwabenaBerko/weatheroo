package com.kwabenaberko.weatheroo.core

import java.util.*

fun String.capitalizeAll(): String{
    return this.split(" ")
        .map { it.capitalize(Locale.ROOT) }
        .joinToString(" ")
}