package com.kwabenaberko.weatheroo.core.presentation

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import com.kwabenaberko.weatheroo.BuildConfig
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class WeatherooApp: Application() {

    override fun onCreate() {
        super.onCreate()

        AndroidThreeTen.init(this)

        if(BuildConfig.DEBUG){
            Timber.plant(Timber.DebugTree())
        }
    }

}