package com.kwabenaberko.weatheroo.core

object Constants {

    const val BASE_URL_DOMAIN = "api.openweathermap.org"
    const val BASE_URL = "https://${BASE_URL_DOMAIN}/"
    const val DATE_FORMAT_PATTERN = "EEEE, dd MMMM, yyyy"

}