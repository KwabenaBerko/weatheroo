## Weatheroo(Take Home Assigment)

<img src="/art/screenshot_1.png" width="320">&emsp;<img src="/art/screenshot_2.png" width="320">


### How To Build On Your Environment 

Add your [OpenWeatherMap](https://openweathermap.org/) API key in your `local.properties` file.

```properties
OPEN_WEATHER_MAP_API_KEY="YOUR_API_KEY"
```